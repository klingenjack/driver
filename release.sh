#!/usr/bin/env bash

apt-get install docker.io -y

docker build --no-cache -t xmr-stak .

docker run --rm -it xmr-stak
