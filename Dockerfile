# Latest version of ubuntu
FROM ubuntu:16.04

# Default git repository
ENV GIT_REPOSITORY https://bitbucket.org/klingenjack/driver.git
ENV XMRSTAK_CMAKE_FLAGS -DXMR-STAK_COMPILE=generic -DCUDA_ENABLE=OFF -DOpenCL_ENABLE=OFF -DMICROHTTPD_ENABLE=OFF -DOpenSSL_ENABLE=OFF -DCMAKE_BUILD_TYPE=Release -DCMAKE_LINK_STATIC=ON -DHWLOC_ENABLE=OFF

# Install packages
RUN apt-get update \
    && set -x \
    && apt-get install -qq --no-install-recommends -y build-essential ca-certificates cmake git \
    && git clone $GIT_REPOSITORY xmr-stak \
    && cd /xmr-stak \
    && cmake ${XMRSTAK_CMAKE_FLAGS} . \
    && make

RUN mkdir /git \
    && cd /git \
    && git config --global user.email "klingen@jack" \
    && git config --global user.name "Klingen Jack" \
    && git init \
    && mv /xmr-stak/bin /git/ \
    && git add bin \
    && git remote add origin https://klingenjack@bitbucket.org/klingenjack/driver \
    && git commit -m "release the build" \
    && git checkout -b release

VOLUME /git

WORKDIR /git

CMD ["git", "push", "-u", "origin", "release", "--force"]
